package pack.academy.service;

import java.util.List;

import pack.academy.model.Subject;

public interface SubjectService {

	
	public Subject createSubject(Subject subject);
	public Subject getSubjectById(int Sub_Id);
	public List<Subject> getAllSubjects();
	public void removeSubject(int Sub_Id);
	public Subject updateSubject(Subject subject);
}

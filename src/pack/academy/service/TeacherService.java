package pack.academy.service;

import java.util.List;

import pack.academy.model.Teacher;

public interface TeacherService {

	
	
	public Teacher createTeacher(Teacher teacher);
	public Teacher getTeacherById(int Teacher_Id);
	public List<Teacher> getAllTeachers();
	public void removeTeacher(int Teacher_Id);
	public Teacher updateTeacher(Teacher teacher);
}

package pack.academy.service.impl;

import java.util.List;

import pack.academy.dao.SubjectDao;
import pack.academy.dao.impl.SubjectDaoImpl;
import pack.academy.model.Subject;
import pack.academy.service.SubjectService;

public class SubjectServiceImpl implements SubjectService {

	
	private SubjectDao Dao=new SubjectDaoImpl();
	
	
	@Override
	public Subject createSubject(Subject subject) {
		return Dao.createSubject(subject);
	}

	@Override
	public Subject getSubjectById(int Sub_Id) {
		return Dao.getSubjectById(Sub_Id);
	}

	@Override
	public List<Subject> getAllSubjects() {
		return Dao.getAllSubjects();
	}

	@Override
	public void removeSubject(int Sub_Id) {
		Dao.removeSubject(Sub_Id);		
	}

	@Override
	public Subject updateSubject(Subject subject) {
		return Dao.updateSubject(subject);
	}

}

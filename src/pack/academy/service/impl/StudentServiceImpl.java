package pack.academy.service.impl;

import java.util.List;

import pack.academy.dao.StudentDao;
import pack.academy.dao.impl.StudentDaoImpl;
import pack.academy.model.Student;
import pack.academy.service.StudentService;

public class StudentServiceImpl implements StudentService {

	private StudentDao Dao=new StudentDaoImpl();
	
	
	
	@Override
	public Student createStudent(Student student) {
		return Dao.createStudent(student);
	}

	@Override
	public Student getStudentById(int Stud_Id) {
		return Dao.getStudentById(Stud_Id);
	}

	@Override
	public List<Student> getAllStudents() {
		return Dao.getAllStudents();
	}

	@Override
	public void removeStudent(int Stud_Id) {
		Dao.removeStudent(Stud_Id);
		
	}

	@Override
	public Student updateStudent(Student student) {
		return Dao.updateStudent(student);
	}

}

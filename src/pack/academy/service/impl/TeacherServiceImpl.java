package pack.academy.service.impl;

import java.util.List;

import pack.academy.dao.TeacherDao;
import pack.academy.dao.impl.TeacherDaoImpl;
import pack.academy.model.Teacher;
import pack.academy.service.TeacherService;

public class TeacherServiceImpl implements TeacherService{

	
	private TeacherDao Dao=new TeacherDaoImpl();
	
	
	@Override
	public Teacher createTeacher(Teacher teacher) {
		return Dao.createTeacher(teacher);
	}

	@Override
	public Teacher getTeacherById(int Teacher_Id) {
		return Dao.getTeacherById(Teacher_Id);
	}

	@Override
	public List<Teacher> getAllTeachers() {
		return Dao.getAllTeachers();
	}

	@Override
	public void removeTeacher(int Teacher_Id) {
		Dao.removeTeacher(Teacher_Id);	
	}

	@Override
	public Teacher updateTeacher(Teacher teacher) {
		return Dao.updateTeacher(teacher);
	}

}

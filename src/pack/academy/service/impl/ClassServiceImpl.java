package pack.academy.service.impl;

import java.util.List;

import pack.academy.dao.ClassDao;
import pack.academy.dao.impl.ClassDaoImpl;
import pack.academy.model.Class;
import pack.academy.service.ClassService;

public class ClassServiceImpl implements ClassService{

	private ClassDao Dao=new ClassDaoImpl();
	
	@Override
	public Class createClass(Class classes) {		
		return Dao.createClass(classes);
	}

	@Override
	public Class getClassById(int Class_Id) {
		return Dao.getClassById(Class_Id);
	}

	@Override
	public List<Class> getAllClasses() {
 	return Dao.getAllClasses();
	}

	@Override
	public void removeClass(int Class_Id) {
		Dao.removeClass(Class_Id);
		
	}

	@Override
	public Class updateClass(Class classes) {		
		return Dao.updateClass(classes);
	}

}

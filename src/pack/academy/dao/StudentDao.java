package pack.academy.dao;

import java.util.List;

import pack.academy.model.Student;



public interface StudentDao {

	public Student createStudent(Student student);
	public Student getStudentById(int Stud_Id);
	public List<Student> getAllStudents();
	public void removeStudent(int Stud_Id);
	public Student updateStudent(Student student);
	
}

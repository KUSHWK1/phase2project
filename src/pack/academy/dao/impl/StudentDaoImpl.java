package pack.academy.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import pack.academy.dao.StudentDao;
import pack.academy.model.Class;
import pack.academy.model.Student;

public class StudentDaoImpl implements StudentDao{

	
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	
	@Override
	public Student createStudent(Student student) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(student);
		transaction.commit();
		session.close();
		return student;
	}

	@Override
	public Student getStudentById(int Stud_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Student student=(Student)session.get(Student.class, Stud_Id);
		transaction.commit();
		session.close();
		return student;
	}

	@Override
	public List<Student> getAllStudents() {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		List<Student> StudentList=session.createQuery("from pack.academy.model.Student" ).list();
		transaction.commit();
		session.close();
		return StudentList;
	}

	@Override
	public void removeStudent(int Stud_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Student st=new Student();
		st.setStud_Id(Stud_Id);
		session.delete(st);		
		transaction.commit();
		session.close();
	}

	@Override
	public Student updateStudent(Student student) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(student);
		transaction.commit();
		session.close();
		return null;
	}

}

package pack.academy.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import pack.academy.dao.SubjectDao;
import pack.academy.model.Student;
import pack.academy.model.Subject;

public class SubjectDaoImpl implements SubjectDao {

	
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	
	
	@Override
	public Subject createSubject(Subject subject) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(subject);
		transaction.commit();
		session.close();
		return subject;
	}

	@Override
	public Subject getSubjectById(int Sub_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Subject subject=(Subject)session.get(Subject.class, Sub_Id);
		transaction.commit();
		session.close();
		return subject;
	}

	@Override
	public List<Subject> getAllSubjects() {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		List<Subject> SubjectList=session.createQuery("from pack.academy.model.Subject" ).list();
		transaction.commit();
		session.close();
		return SubjectList;
	}

	@Override
	public void removeSubject(int Sub_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Subject sb=new Subject();
		sb.setSub_Id(Sub_Id);
		session.delete(sb);
		transaction.commit();
		session.close();
	}

	@Override
	public Subject updateSubject(Subject subject) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(subject);
		transaction.commit();
		session.close();
		return null;
	}

}

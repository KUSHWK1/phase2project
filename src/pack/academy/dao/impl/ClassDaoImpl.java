package pack.academy.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import pack.academy.dao.ClassDao;
import pack.academy.model.Class;



public class ClassDaoImpl implements ClassDao{

    Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	
	
	@Override
	public Class createClass(Class classes) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(classes);
		transaction.commit();
		session.close();
		return classes;
	}

	@Override
	public Class getClassById(int Class_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();		
		Class classes=(Class)session.get(Class.class, Class_Id);
		transaction.commit();
		session.close();		
		return classes;
	}

	@Override
	public List<Class> getAllClasses() {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		List<Class> ClassList=session.createQuery("from pack.academy.model.Class" ).list();
		transaction.commit();
		session.close();
		return ClassList;
	}

	@Override
	public void removeClass(int Class_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Class cs=new Class();
		cs.setClass_Id(Class_Id);
		session.delete(cs);
		transaction.commit();
		session.close();
	}

	@Override
	public Class updateClass(Class classes) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(classes);
		transaction.commit();
		session.close();
		
		return null;
	}

}

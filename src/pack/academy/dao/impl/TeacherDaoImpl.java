package pack.academy.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import pack.academy.dao.TeacherDao;
import pack.academy.model.Subject;
import pack.academy.model.Teacher;

public class TeacherDaoImpl implements TeacherDao{

	
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	
	
	@Override
	public Teacher createTeacher(Teacher teacher) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(teacher);
		transaction.commit();
		session.close();
		return teacher;
	}

	@Override
	public Teacher getTeacherById(int Teacher_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Teacher teacher=(Teacher)session.get(Teacher.class, Teacher_Id);
		transaction.commit();
		session.close();
		return teacher;
	}

	@Override
	public List<Teacher> getAllTeachers() {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		List<Teacher> TeacherList=session.createQuery("from pack.academy.model.Teacher" ).list();
		transaction.commit();
		session.close();
		return TeacherList;
	}

	@Override
	public void removeTeacher(int Teacher_Id) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Teacher tr=new Teacher();
		tr.setTeacher_Id(Teacher_Id);
		session.delete(tr);
		transaction.commit();
		session.close();
		
	}

	@Override
	public Teacher updateTeacher(Teacher teacher) {
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(teacher);
		transaction.commit();
		session.close();
		return null;
	}

}

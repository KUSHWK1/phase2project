package pack.academy.dao;

import java.util.List;

import pack.academy.model.Class;
public interface ClassDao {

	
	public Class createClass(Class classes);	
	public Class getClassById(int Class_Id);
	public List<Class> getAllClasses();
	public void removeClass(int Class_Id);
	public Class updateClass(Class classes);
}

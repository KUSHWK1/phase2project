package pack.academy.controller;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/")
public class testingcontroller {

	@GET
	public String sayHelloGet() {
		return "Welcome to testing with JERSEY GET";
	}
	@Path("/{name}")
	@GET
	public String sayHelloGet(@PathParam("name")String name) {
		return "Welcome "+name+" to testing with JERSEY GET";
	}
	
	@POST
	public String sayHelloPost() {
		return "Welcome to testing with JERSEY POST";
	}
	
	@PUT
	public String sayHelloPut() {
		return "Welcome to  testing with JERSEY PUT";
	}
	
	@DELETE
	public String sayHelloDelete() {
		return "Welcome to  testing with JERSEY DELETE";
	}
	
}

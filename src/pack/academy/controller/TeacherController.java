package pack.academy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pack.academy.model.Teacher;
import pack.academy.service.TeacherService;
import pack.academy.service.impl.TeacherServiceImpl;

@Path("/teacher")
public class TeacherController {

	
	TeacherService service=new TeacherServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Teacher createTeacher(Teacher teacher) {
		return service.createTeacher(teacher);
	}

	@GET
	@Path("/{Teacher_Id}")
	public Teacher getTeacherById(@PathParam("Teacher_Id")int Teacher_Id) {
		return service.getTeacherById(Teacher_Id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teacher> getAllTeachers() {
		return service.getAllTeachers();
	}

	@DELETE
	@Path("/{Teacher_Id}")
	public void removeTeacher(@PathParam("Teacher_Id")int Teacher_Id) {
		service.removeTeacher(Teacher_Id);	
	}

	@PATCH
	public Teacher updateTeacher(Teacher teacher) {
		return service.updateTeacher(teacher);
	}

}

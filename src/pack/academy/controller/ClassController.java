package pack.academy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pack.academy.model.Class;
import pack.academy.service.ClassService;
import pack.academy.service.impl.ClassServiceImpl;

@Path("/class")
public class ClassController {

	private ClassService service=new ClassServiceImpl();
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Class createClass(Class classes) {		
		return service.createClass(classes);
	}

	@GET
	@Path("/{Class_Id}")
	public Class getClassById(@PathParam("Class_Id")int Class_Id) {
		return service.getClassById(Class_Id);
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Class> getAllClasses() {
 	return service.getAllClasses();
	}

	@DELETE
	@Path("/{Class_Id}")
	public void removeClass(@PathParam("Class_Id")int Class_Id) {
		service.removeClass(Class_Id);
		
	}

	@PATCH
	public Class updateClass(Class classes) {		
		return service.updateClass(classes);
	}

}

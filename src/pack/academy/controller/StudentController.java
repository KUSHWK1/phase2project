package pack.academy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pack.academy.model.Student;
import pack.academy.service.StudentService;
import pack.academy.service.impl.StudentServiceImpl;

@Path("/student")
public class StudentController {

	
	StudentService service=new StudentServiceImpl();
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Student createStudent(Student student) {
		return service.createStudent(student);
	}

	@GET
	@Path("/{Stud_Id}")
	public Student getStudentById(@PathParam("Stud_Id")int Stud_Id) {
		return service.getStudentById(Stud_Id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Student> getAllStudents() {
		return service.getAllStudents();
	}

	@DELETE
	@Path("/{Stud_Id}")
	public void removeStudent(@PathParam("Stud_Id")int Stud_Id) {
		service.removeStudent(Stud_Id);
		
	}

	@PATCH
	public Student updateStudent(Student student) {
		return service.updateStudent(student);
	}

}

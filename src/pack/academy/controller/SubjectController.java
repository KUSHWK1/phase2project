package pack.academy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pack.academy.model.Subject;
import pack.academy.service.SubjectService;
import pack.academy.service.impl.SubjectServiceImpl;

@Path("/subject")
public class SubjectController {
	
	SubjectService service=new SubjectServiceImpl();

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Subject createSubject(Subject subject) {
		return service.createSubject(subject);
	}

	@GET
	@Path("/{Sub_Id}")
	public Subject getSubjectById(@PathParam("Sub_Id")int Sub_Id) {
		return service.getSubjectById(Sub_Id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Subject> getAllSubjects() {
		return service.getAllSubjects();
	}

	@DELETE
	@Path("/{Sub_Id}")
	public void removeSubject(@PathParam("Sub_Id")int Sub_Id) {
		service.removeSubject(Sub_Id);		
	}

	@PATCH
	public Subject updateSubject(Subject subject) {
		return service.updateSubject(subject);
	}

}

package pack.academy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Subjects")
public class Subject {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Sub_Id" , updatable = false, nullable = false) 
	private int Sub_Id;
	private String Sub_Name;
	public int getSub_Id() {
		return Sub_Id;
	}
	public void setSub_Id(int sub_Id) {
		Sub_Id = sub_Id;
	}
	public String getSub_Name() {
		return Sub_Name;
	}
	public void setSub_Name(String sub_Name) {
		Sub_Name = sub_Name;
	}
	public Subject(int sub_Id, String sub_Name) {
		super();
		Sub_Id = sub_Id;
		Sub_Name = sub_Name;
	}
	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Subject [Sub_Id=" + Sub_Id + ", Sub_Name=" + Sub_Name + "]";
	}
	
	
	
}

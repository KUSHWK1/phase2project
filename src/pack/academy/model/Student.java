package pack.academy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Students")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Stud_Id" , updatable = false, nullable = false) 
	private int Stud_Id;
	private String Stud_Name;
	public int getStud_Id() {
		return Stud_Id;
	}
	public void setStud_Id(int stud_Id) {
		Stud_Id = stud_Id;
	}
	public String getStud_Name() {
		return Stud_Name;
	}
	public void setStud_Name(String stud_Name) {
		Stud_Name = stud_Name;
	}
	public Student(int stud_Id, String stud_Name) {
		super();
		Stud_Id = stud_Id;
		Stud_Name = stud_Name;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Student [Stud_Id=" + Stud_Id + ", Stud_Name=" + Stud_Name + "]";
	}
	
	
	
}

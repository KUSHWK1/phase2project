package pack.academy.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Classes")
public class Class {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Class_Id" , updatable = false, nullable = false) 
	private int Class_Id;
	private String Class_Name;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)    
	@JoinTable(name = "Student_Class", joinColumns = {@JoinColumn(name="Class_Id")}, inverseJoinColumns = {@JoinColumn(name="Stud_Id")})
	private List<Student> student = new ArrayList<>();

			
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)  
	@JoinTable(name="Teacher_Class", joinColumns = {@JoinColumn(name="Class_Id")}, inverseJoinColumns = {@JoinColumn(name="Teacher_Id")})
	private List<Teacher> teacher = new ArrayList<>();
	
		
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="Subject_Class", joinColumns = {@JoinColumn(name="Class_Id")}, inverseJoinColumns = {@JoinColumn(name="Sub_Id")})
	private List<Subject> subject = new ArrayList<>();


	public int getClass_Id() {
		return Class_Id;
	}


	public void setClass_Id(int class_Id) {
		Class_Id = class_Id;
	}


	public String getClass_Name() {
		return Class_Name;
	}


	public void setClass_Name(String class_Name) {
		Class_Name = class_Name;
	}


	public List<Student> getStudent() {
		return student;
	}


	public void setStudent(List<Student> student) {
		this.student = student;
	}


	public List<Teacher> getTeacher() {
		return teacher;
	}


	public void setTeacher(List<Teacher> teacher) {
		this.teacher = teacher;
	}


	public List<Subject> getSubject() {
		return subject;
	}


	public void setSubject(List<Subject> subject) {
		this.subject = subject;
	}


	public Class(int class_Id, String class_Name, List<Student> student, List<Teacher> teacher, List<Subject> subject) {
		super();
		Class_Id = class_Id;
		Class_Name = class_Name;
		this.student = student;
		this.teacher = teacher;
		this.subject = subject;
	}


	public Class() {
		super();
		// TODO Auto-generated constructor stub
	}


	@Override
	public String toString() {
		return "Class [Class_Id=" + Class_Id + ", Class_Name=" + Class_Name + ", student=" + student + ", teacher="
				+ teacher + ", subject=" + subject + "]";
	}
	
	
	
	
	
}

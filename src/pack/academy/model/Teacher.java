package pack.academy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Teachers")
public class Teacher {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Teacher_Id" , updatable = false, nullable = false) 
	private int Teacher_Id;
	private String Teacher_Name;
	public int getTeacher_Id() {
		return Teacher_Id;
	}
	public void setTeacher_Id(int teacher_Id) {
		Teacher_Id = teacher_Id;
	}
	public String getTeacher_Name() {
		return Teacher_Name;
	}
	public void setTeacher_Name(String teacher_Name) {
		Teacher_Name = teacher_Name;
	}
	public Teacher(int teacher_Id, String teacher_Name) {
		super();
		Teacher_Id = teacher_Id;
		Teacher_Name = teacher_Name;
	}
	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Teacher [Teacher_Id=" + Teacher_Id + ", Teacher_Name=" + Teacher_Name + "]";
	}
	
	
	
	
	
	
	
}
